#include <factorial.h>

unsigned factorial (unsigned x) {
  unsigned result = 1;
  while (x > 1) {
    unsigned new_result = result * x;
    if (new_result < result) {
      return 0;
    }
    result = new_result;
    --x;
  }
  return result;
}
