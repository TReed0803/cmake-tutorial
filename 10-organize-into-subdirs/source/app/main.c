#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <names.h>

int main () {
  srand((unsigned) time(NULL));
  printf("%s\n", kFirstNames[(clock() * rand()) % kFirstNamesSize]);
  return 0;
}
