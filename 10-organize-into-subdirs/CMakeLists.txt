################################################################################
# This project moves things around a bit and puts them in various directories.
# For this build, lets try configuring using the CMake GUI (yup, it has one)!
#
# If you have installed on Windows, it should be available in your applications
# menu. If you are on Linux, the GUI is often a separate package (on Ubuntu,
# this package is called "cmake-qt-gui").
#
# Open up the application and follow these instructions:
# 1.  Click "Browse Source...", find this directory (10-organize-into-subdirs).
# 2.  Click "Browse Build...", find a build directory to configure into.
# 3.  Click "Configure" at the bottom-left.
#     a.  The GUI will prompt your for what kind of project to generate.
#         Select whatever you think is appropriate, usually you want the default
#         options.
# 4.  The table in the center has configuration options. We haven't talked about
#     these yet, but we will get to them soon. For now, you can look at them -
#     but you probably shouldn't change any of them.
# 5.  Click "Generate" at the bottom-left.
# 6.  If you have built a project that can be opened (e.g. Visual Studio
#     solutions), you can now just click "Open Project". Otherwise, you may have
#     to navigate to your project manually.
#
# This is the last novel way to configure a CMake project ;_; - Use whichever
# you find most convenient. For me, I tend to be in the terminal a lot, so I opt
# for the `cmake /path/to/source` version quite often. It's up to you!
################################################################################
cmake_minimum_required(VERSION 3.16)

# From this point forward, I will not really comment about the project command.
# Aside from some fringe additional features, you know all you need to know.
project(
  #[[NAME]]    NameGenerator
  VERSION      1.0
  DESCRIPTION  "Generates a first and last name combination."
  LANGUAGES    C
)

# There are two things of note here;
# 1.  Files provided to these functions are relative from the CMakeLists.txt
#     file executing, and may be in other directories.
# 2.  names.h is now in a different directory from main.c... We need a way to
#     suggest to CMake which directories `add_executable` can include.
#
# Actually, the files *could* be absolute paths if needed. But this isn't needed
# here, so this seems simpler to me.
add_library(names STATIC source/lib/names.c source/lib/names.h)

# There are really two ways to suggest that targets should include a directory.
# You can either do it per-target, or you can do it per-directory. So I could
# say, for example, that all targets in this directory should include a given
# directory when building.
#
#   include_directories(source/lib)
#
# YOU SHOULD AVOID THIS!
#
# It is much more expressive and meaningful to put the property for what should
# and should not be included on the target itself. In general, whenever CMake
# gives you the option of doing something specifically to a target versus doing
# something more globally, you should prefer acting on the target.
#
#   target_include_directories(<target-name> [PUBLIC|PRIVATE|INTERFACE] <paths>)
#
# This is the command that you should call. And while you can optionally omit
# the [PUBLIC|PRIVATE|INTERFACE] part - you should not. As always, be *more*
# specific, not less.
#
# So what do these visibilities mean anyways? Well, they control who inherits
# these provided include directories.
#
#    PUBLIC    := All linked targets inherit the provided directories.
#    PRIVATE   := Only the current target has the provided directories.
#    INTERFACE := Only targets depending on this target inherit the directories.
#
# There are a few extra things you can do with this function, but this is the
# most common thing to be aware of. In this case, we don't need the header file
# in names.c, but whoever wants to use this library does - so INTERFACE.
target_include_directories(names INTERFACE source/lib)

# Nothing actually needs to change for this to still work, however I have added
# a visibility specification to the link step here. This works similarly to the
# target_include_directories.
#
#    PUBLIC    := Target is linked, and interface data is inherited.
#    PRIVATE   := Target is linked, but interface data is not inherited.
#    INTERFACE := Target is not linked, but interface data is inherited.
#
# In order to understand why this is useful, I have to explain something. In
# CMake library targets can be linked with one-another (even though that doesn't
# really make sense). What this means is that there is a dependency.
#
# So for example:
#
#   add_library(foo ...)
#   add_library(bar ...)
#   target_link_libraries(foo bar)
#
#   add_executable(baz ...)
#   target_link_libraries(baz foo)  # Links to foo AND bar.
#
# So, still confused on PUBLIC, PRIVATE, and INTERFACE? Well, I think they make
# the most sense when we talk about library targets. So thinking about it from
# that perspective, let's consider the example of foo, bar, and baz above...
#
# 1.  If you PUBLIC included a directory on target foo, we know that it means
#     bar inherits this include.
# 2.  Do we want baz to inherit this include as well?
#
# This is basically the question you are answering with these visibility specs.
# Notice, that it actually applies to more than just includes. You can also add
# preprocessor definitions to a target (which you may wish to control the
# visibility of in a similar way).
add_executable(name-generator source/app/main.c)
target_link_libraries(name-generator PRIVATE names)
