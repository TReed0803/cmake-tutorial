#include <stddef.h>

char const * kFirstNames[] = {
  "Adeleine",
  "Galacta Knight",
  "King Dedede",
  "Kirby",
  "Magolor",
  "Marx",
  "Meta Knight",
  "Void Termina"
};

size_t kFirstNamesSize = sizeof(kFirstNames) / sizeof(kFirstNames[0]);
