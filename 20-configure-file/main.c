#include <stdio.h>

#include "version.h"

int main () {
  printf("%s (%s) - %s\n", PROJECT_NAME, PROJECT_VERSION, PROJECT_DESCRIPTION);
  printf("Sentinel variable is: %s\n", SENTINEL);
  printf(
    "Numeric Version Representation: %u.%u.%u.%u\n",
    PROJECT_VERSION_MAJOR,
    PROJECT_VERSION_MINOR,
    PROJECT_VERSION_PATCH,
    PROJECT_VERSION_TWEAK
  );
  return 0;
}
