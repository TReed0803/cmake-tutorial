#include <stdio.h>
#include "foo.h"

void bar (void) {
  puts("bar begin");
  foo();
  puts("bar end");
}
