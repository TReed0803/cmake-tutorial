#include <stdio.h>
#include <stdlib.h>

int main (int argc, char const * argv[]) {
  if (argc <= 1) {
    puts("Fine, I didn't want to play anyways.");
    return 0;
  }
  if (argc > 2) {
    puts("Sorry, not feeling up to it right now.");
    return 1;
  }
  int limit = atoi(argv[1]);
  for (int idx = 0; idx <= limit; ++idx) {
    if (idx % 3 == 0 && idx % 5 == 0) {
      fputs("FizzBuzz ", stdout);
    }
    else if (idx % 3 == 0) {
      fputs("Fizz ", stdout);
    }
    else if (idx % 5 == 0) {
      fputs("Buzz ", stdout);
    }
    else {
      fprintf(stdout, "%d ", idx);
    }
  }
  return 0;
}
