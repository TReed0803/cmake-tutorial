#include "speak.h"
#include <stdio.h>

int main (int argc, char const * argv[]) {
  for (int idx = 1; idx < argc; ++idx) {
    if (speak_check(argv[idx])) {
      puts("You may enter.");
      return 0;
    }
  }
  return 1;
}
