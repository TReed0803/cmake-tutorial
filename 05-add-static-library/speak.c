#include <string.h>

int speak_check (char const * text) {
  return strcmp(text, "friend") == 0;
}
