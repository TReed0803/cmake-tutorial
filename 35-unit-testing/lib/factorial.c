#include <factorial.h>

unsigned factorial (unsigned x) {
  unsigned result = 1;
  while (x > 1) {
    result *= x;
    --x;
  }
  return result;
}
