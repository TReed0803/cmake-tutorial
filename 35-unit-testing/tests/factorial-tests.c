#include <factorial.h>

#include <stdio.h>

static int errors = 0;

// -----------------------------------------------------------------------------
#define EXPECT_OP(actual, op, expected) do {                                    \
  const int _actual = (actual);                                                 \
  const int _expected = (expected);                                             \
  if(!(_actual op _expected)) {                                                 \
    ++errors;                                                                   \
    printf(                                                                     \
      "EXPECT(%d): Expected(" #op "): %d, Actual %d\n",                         \
      __LINE__,                                                                 \
      _expected,                                                                \
      _actual                                                                   \
    );                                                                          \
  }                                                                             \
} while(0)

// -----------------------------------------------------------------------------
#define EXPECT_EQ(actual, expected) EXPECT_OP(actual, ==, expected)

// -----------------------------------------------------------------------------
int main () {
  EXPECT_EQ(factorial(0), 1);
  EXPECT_EQ(factorial(1), 1);
  EXPECT_EQ(factorial(2), 2);
  EXPECT_EQ(factorial(3), 6);
  EXPECT_EQ(factorial(4), 24);
  EXPECT_EQ(factorial(5), 120);
  return (errors == 0) ? 0 : 1;
}
