# The important thing to note about this example is that the binary doesn't have
# to be a binary that you intend to release. You could very well just make a
# binary just to execute some more fine-grained unit tests on a library.
add_executable(factorial-tests factorial-tests.c)
target_link_libraries(factorial-tests PRIVATE factorial)

# Since add_test expects the test to pass (by returning 0), as long as your unit
# test infrastructure returns the proper status code, this is all you need to
# integrate unit tests with CTest.
add_test(NAME factorial COMMAND factorial-tests)
