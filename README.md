# CMake Tutorial

The purpose of this repository is to demonstrate an effective introduction into
the world of CMake. CMake is a tool for abstracting the build system so that
builds can be occur on a variety of platforms and configurations.

## How to Use This Repository

Each directory within this repository is named like so:

  `##-<what-is-learned>`

The purpose of this is to provide some order to traverse the samples. The
samples will not have their own README.md file, the expectation is that you will
learn all you need to learn from the corresponding CMakeLists.txt files.

Until you get the hang of the structure, I recommend starting with the first
sample and moving through them in ascending order.

## Disclaimer

This was hacked together in a matter of a few hours. I got really messy with
some of the non-important code (like the C or C++ code that I didn't really
expect anyone reading this to spend much time on). So please, be aware that
while I try to keep this code relatively clean, parts of it will be not great.

## Some Required Reading

### The First Line of Code You'll See

If you're working on a CMake project, the very first line of code you'll see is:

```cmake
cmake_minimum_required(VERSION 3.16)
```

Optionally, this command can be specified as a range of minimum versions:

```cmake
cmake_minimum_required(VERSION 3.16...3.19)
```

What this line says is basically "this project supports at-minimum CMake 3.16 -
it is tested to work up-to 3.19." Backwards compatibility is enforced through
this call, it sets the cmake-policy for various execution policies.

If a user is on a version lower than this value, the process stops here and
emits an error. If a user is within the range of this value, CMake will act like
that version. And if a user is above of the range of this value, CMake will be
configured to act like the maximum-supported version.

You tend to want to be on newer versions of CMake. Both for access to newer
functions and features, and sometimes for efficiency reasons. If you want to
bump the minimum version number that is a question that is team-dependent. If
you want to read more about exactly what this call does you can read the
documentation:

https://cmake.org/cmake/help/latest/command/cmake_minimum_required.html

This is heavily related to:

https://cmake.org/cmake/help/latest/command/cmake_policy.html

Which basically configures CMake to act differently with these policies:

https://cmake.org/cmake/help/latest/manual/cmake-policies.7.html

### General Call Syntax

CMake call syntax looks something like this:

```cmake
function_name(ARG1 ARG2 [...] ARGN)
```

Note the distinct lack of commas? The syntax feels a lot like bash or shell to
me. Some folks complain about this, and I see where it comes from. But
practically it rarely presents a problem. Personally, I think it's fine - I
rarely have an issue with the language itself (though there are some things I
dislike, we'll briefly touch on those soon).

Some functions in CMake are very targeted, they do one thing and their signature
is simple. Here are some simple function signatures. You could probably tell
what they do by their name and arguments.

```cmake
set(variable_name "VALUE")
message(STATUS "Hello, World!")
file(WRITE "filename" "contents")
```

Some function return values, for example `string` function has a `FIND` command
for finding a sub-string within a provided string.

```cmake
string(FIND "this is a string" "is a" output_variable)
# output_variable now contains 5. If not found, it would have contained -1.
```

### Control Parameters

This brings us to one of the patterns in CMake functions: control parameters.

Or well, I call them control parameters. I don't think there is an official name
for these things. As far as the CMake language is concerned, `FIND` is just a
normal parameter to the function `string`. However, it's a well-established
pattern in CMake to make these all-caps words represent parameters which control
how the function acts - hence, "control parameters".

Here are some very common function which use control parameters:

*   https://cmake.org/cmake/help/latest/command/file.html
*   https://cmake.org/cmake/help/latest/command/list.html
*   https://cmake.org/cmake/help/latest/command/message.html
*   https://cmake.org/cmake/help/latest/command/string.html

You'll notice that almost all functions which have these kinds of parameters
format them the same way. All with `UPPER_SNAKE_CASE`. You should do the same,
if you make your own functions.

### Parameter Order

For many functions, the first few parameter orders matter. They usually kind of
"namespace" other related commands. So like, we don't know what other parameters
we need to provide to `string` until we see what the first control parameter is.

Some functions have complex call signatures, and may be lax with their order.
Usually these are larger functions, which have many optional control parameters.
Usually it works in a similar way, where the first few control parameters have
a required order, but after that it doesn't matter.

For example, the following functions calls are all identical:

```cmake
execute_process(
  COMMAND ${CMAKE_COMMAND} -E echo "Hello, World!"
  RESULT_VARIABLE result
  OUTPUT_FILE     "output.log"
  ERROR_VARIABLE  error_output
)
execute_process(
  COMMAND ${CMAKE_COMMAND} -E echo "Hello, World!"
  OUTPUT_FILE     "output.log"
  ERROR_VARIABLE  error_output
  RESULT_VARIABLE result
)
execute_process(
  COMMAND ${CMAKE_COMMAND} -E echo "Hello, World!"
  OUTPUT_FILE     "output.log"
  RESULT_VARIABLE result
  ERROR_VARIABLE  error_output
)
```

This brings us to our first big negative of CMake...

### It Can Be Hard to Recall Parameter Order Sometimes

The fact that some parameters are positional, and some parameters aren't, and
every function was implemented to handle parameters in its own way, means that
it can be really hard to remember the exact order that parameters have to be in
for a function.

This is why I really, *highly recommend* becoming very adept with the CMake
online documentation. I literally love this tool/language, and even I cannot
remember the order to parameters for things all the time. You occasionally have
things which happen in an order different from what most other functions do, and
it can really throw a wrench in your work.

So when reaching for a command to call, if you aren't *certain* the order of the
parameters, just quickly look it up.

Pro-tip: If you're on a page like this:

https://cmake.org/cmake/help/latest/command/execute_process.html

All you need to do is change the last part to the command in question:

https://cmake.org/cmake/help/latest/command/string.html

It might also be wise to change to your minimum supported version:

https://cmake.org/cmake/help/v3.16/command/string.html

## The CMake Language

If you feel uncomfortable with the CMake langauge, you may wish to also go
through this section of the README. As always, the CMake documentation is
fantastic, so if you have any questions - that takes priority to my statements.

https://cmake.org/cmake/help/latest/manual/cmake-language.7.html

NOTE: If you want to execute some sample code, simply place it in a file, and
then run the file with cmake like so:

  cmake -P /path/to/file.cmake

### You Don't Declare Variables

If you could imagine an infinite set of all valid variable names, by default all
of them are **UNSET** (well, aside from the ones set for you by CMake).

When you call `set` you are putting the variable into a SET state with the
provided value (value optional).

```cmake
set(variable_name "My Value")
```

In order to access the contents of a variable, you have to expand it:

```cmake
message(STATUS "The variable has the value ${variable_name}")
```

If you want to see if a variable is SET or not, you can ask:

```cmake
if(DEFINED variable_name)
  message(STATUS "The variable is defined.")
else()
  message(STATUS "The variable is not defined.")
endif()
```

If you want to invert this, you can with the `NOT` control parameter:

```cmake
if(NOT DEFINED variable_name)
  message(STATUS "The variable is not defined.")
endif()
```

Notice that a variable can be DEFINED, but empty:

```cmake
set(variable_name "")
if(DEFINED variable_name)
  message(STATUS "The variable is defined.")  # This prints
else()
  message(STATUS "The variable is not defined.")
endif()
```

And though you don't need to do this, if you want to unset a variable for
whatever reason, you can:

```cmake
unset(variable_name)
```

NOTE: If you are curious about what else you can pass to the if-command, please
see the relevant documentation. It's worth a read, I promise.

### All Variables Are Lists of Strings

That's right. There are no other types. And implicitly everything is a list.

In our example above, the list is just a list of size 1:

```cmake
set(variable_name "My Value")
list(LENGTH variable_name list_length)
message(STATUS "List of size ${list_length}")  # Prints: "List of size 1"
```

If you want to initialize a variable with a list of a different size, you only
need to add more parameters:

```cmake
set(variable_name "My Value" "Foo" "Bar")
list(LENGTH variable_name list_length)
message(STATUS "List of size ${list_length}")  # Prints: "List of size 3"
```

One other interesting property is that expanding a list of variables expands
into the parameter list (unless you quote them). For example:

```cmake
set(call_params "Hello, World!" RESULT_VARIABLE output)

# Prints "Hello, World!" and sets the process result in the variable `output`.
execute_process(
  COMMAND echo ${call_params}
)

# Same as the above example!
execute_process(
  COMMAND ${CMAKE_COMMAND} -E echo "Hello, World!"
  RESULT_VARIABLE output
)

# This is different, because we quoted the expansion.
# Prints "Hello World!;RESULT_VARIABLE;output", and doesn't set a variable.
execute_process(
  COMMAND ${CMAKE_COMMAND} -E echo "${call_params}"
)
```

This clues us in to the internal format of a string list. A string list itself
is simply a string, where each element is semicolon-delimited. So an alternative
way to produce a string list could be something like this:

```cmake
set(my_list "foo;bar;baz")
```

### Please Understand the If Command

The if-command is one of the most powerful and often-misused commands in CMake.
I don't think I'll do the documentation justice here, just read it:

https://cmake.org/cmake/help/latest/command/if.html

### Coding Conventions

Here, I'm going to talk about my personal coding conventions. You should always
default to whatever your team does - but I wanted to explain why I do what I do,
so that you'll have an easier time reading my code.

CMake commands (the general name for functions or macros) aren't case-sensitive.
So technically, I could call the `message` command in any of these ways:

```cmake
message(STATUS "Wow!")
Message(STATUS "Wow!")
MeSsAgE(STATUS "Wow!")
MESSAGE(STATUS "Wow!")
```

Some early projects tended to prefer all-caps. But I never really liked that,
and it seems like most of the community doesn't like that either because folks
are moving away from this (it tends to be an easy way to spot an old project
though).

Variable names on the other hand, *are* case-sensitive. For this, I have some
slightly complicated rules...

1.  If the variable is a global or cache variable for public use, it is
    `UPPER_SNAKE_CASE`.
2.  If the variable is a global variable for a private use, it is
    `__UPPER_SNAKE_CASE_WITH_TWO_UNDERSCORE_PREFIX`.
3.  If the variable is a local variable within a function scope, it's
    `lower_snake_case`.
4.  If the variable is a local variable within a macro, it's
    `__lower_case_snake_with_two_underscore_prefix`.
5.  If the variable is a function or macro parameter, it's
    `_UPPER_SNAKE_CASE_WITH_ONE_UNDERSCORE_PREFIX`.

You don't have to follow these rules - in fact, these are not widely adopted.
These are just the standards I've personally developed over the years working
with CMake. I have some good reasons for these rules, too.

1.  All other cache/global variables are already `UPPER_SNAKE_CASE`.
2.  It's a common pattern to denote makeshift visibility by prefixing a name
    with multiple underscores (for example, in gcc, symbols with `__` prefix are
    compiler internals, and should be used only by the compiler developers).
3.  I think there is some value in telling apart the local function variables
    from the global variables.
4.  Macros don't have scope, so we prefix with double-underscore when setting a
    variable from a macro so that we can't stomp on some other variable.
5.  And finally... `_PARAM1` is a good way to denote positional parameters,
    because it matches nicely with a prefix-less `cmake_parse_arguments` call.
    (You'll see more about this later.)

### About Functions

Here is an example function declaration.

```cmake
function(my_function)
  message(STATUS "Provided ${ARGC} parameters:")
  foreach(param IN LISTS ARGV)
    message(STATUS "Provided Param: ${param}")
  endforeach()
endfunction()

my_function(foo bar baz)
```

Optionally, you can provide a list of parameter names. When you do this, it
defines the *minimum* number of required parameters. The user can still provide
more parameters past the minimum, though.

```cmake
function(my_function _PARAM_A _PARAM_B _PARAM_C)
  message(STATUS "Positional Param A: ${_PARAM_A}")
  message(STATUS "Positional Param B: ${_PARAM_B}")
  message(STATUS "Positional Param C: ${_PARAM_C}")

  message(STATUS "About to print all parameters (including positional):")
  foreach(param IN LISTS ARGV)
    message(STATUS "Provided Param: ${param}")
  endforeach()

  message(STATUS "About to print extra parameters (excluding positional):")
  foreach(param IN LISTS ARGN)
    message(STATUS "Provided Param: ${param}")
  endforeach()
endfunction()

my_function(foo bar baz bin bap bow)

# Note that the following function will fail with error:
my_function(not_enough parameters)
```

The way values are returned from functions is by providing the function with a
variable name, and then having the function set the variable in the parent's
scope.

```cmake
function(make_exciting _VARIABLE _INPUT)
  set(${_VARIABLE} "${_INPUT}!!" PARENT_SCOPE)
endfunction()

make_exciting(text "This is cool")
message(STATUS "${text}")  # Prints "This is cool!!"
```

Note that you have to provide the `PARENT_SCOPE` control parameter here. If you
don't, the variable will be defined locally to the function, and will not be
available to the caller when you return.

Also note how we used a variable to name the variable we wanted to set? This is
pretty standard CMake stuff. You could imagine returning multiple variables this
way.

### About Macros

Yes, CMake has macros. Sometimes they're necessary, but much like in C and C++
you should avoid them. There are huge differences between functions and macros
in CMake, despite them looking similar.

In general, I'd say you should prefer functions almost always. Unless you have a
really good technical reason to prefer a macro (they exist).

The best way to think about macros is that it is basically like the C
preprocessor. Macros *look* like function definitions, but they don't have any
scope. They are like copying-pasting text right into the target location.

```cmake
macro(my_macro)
  set(output "Boo!")
endmacro()

set(output "I'm going to scare you...")
my_macro()
message(STATUS "${output}") # Prints: "Boo!"
```

You *can* provide parameters to macros, but they don't work like normal function
parameters. A good rule of thumb is to remember the following:

*   All input parameters provided to macros, whether positional or additional,
    *SHOULD BE* expanded in order to use them.

```cmake
macro(my_macro _VARIABLE _INPUT)
  set(${_VARIABLE} "${_INPUT}")
endmacro()

set(output "I'm going to scare you...")
my_macro(output "Boo!")
message(STATUS "${output}") # Prints: "Boo!"
```

Here is an example to illustrate what I mean about that note above.

```cmake
function(my_function _PARAM_A)
  # CMake knows to treat _PARAM_A as a variable, and expand it in this case.
  if(_PARAM_A STREQUAL "HELLO")
    message(STATUS "Hello 1!")
  endif()
endfunction()

macro(my_macro _PARAM_A)
  # BUG: You cannot actually do this. This is actually read as:
  #  if("_PARAM_A" STREQUAL "HELLO"), which is never true...
  if(_PARAM_A STREQUAL "Hello")
    message(STATUS "Hello 2!")
  endif()
endmacro()

my_function(HELLO) # Prints: "Hello 1!"
my_macro(HELLO)    # Doesn't print anything :(
```

`_PARAM_A` isn't an actual variable - it's a substitution name. So you *MUST*
expand the variable to use the value. There isn't actually a variable named
`_PARAM_A` within the macro's scope.

This can be the source of many bugs and great confusion if you aren't careful.

### Control Parameters

If you want to make a function which uses control parameters, you can (and
should) use the provided `cmake_parse_arguments` function. This is where I think
my naming convention for parameters will start to make sense.

What `cmake_parse_arguments` does is takes some input for different kinds of
control parameters (ones which act like flags, ones which can have one value,
and ones which can have multiple values). It also takes a special prefix string.
When a control parameter is found, the data associated with it is set to a
variable named: `${PREFIX}_${CONTROL_PARAMETER_NAME}`.

```cmake
function(control_parameter_example)
  # Note: #[[<text>]] is just a comment that ends at the ]]; kinda like /**/
  # I sometimes like to mark-up functions like this to make them read better.
  cmake_parse_arguments(
    PARSE_ARGV   0
    #[[PREFIX]]  "MY_PREFIX"
    #[[OPTIONS]] "OPTION_A;OPTION_B"
    #[[ONEVAL]]  "FOO;BAR;BAZ"
    #[[MULVAL]]  "MULTI;WOW"
  )

  # For setting flags on the function.
  if(MY_PREFIX_OPTION_A)
    message(STATUS "OPTION_A is set.")
  endif()
  if(MY_PREFIX_OPTION_B)
    message(STATUS "OPTION_B is set.")
  endif()

  # Because these are ONEVAL parameters, they can have no more than 1 value.
  message(STATUS "FOO := ${MY_PREFIX_FOO}")
  message(STATUS "BAR := ${MY_PREFIX_BAR}")
  message(STATUS "BAZ := ${MY_PREFIX_BAZ}")

  # Because these are MULVAL parameters, they can have multiple values.
  foreach(value IN LISTS MY_PREFIX_MULTI)
    message(STATUS "Multi Value: ${value}")
  endforeach()
  foreach(value IN LISTS MY_PREFIX_WOW)
    message(STATUS "Multi Value: ${value}")
  endforeach()
endfunction()

# Example calling it. Note you don't have to provide all parameters?
# Parameters which aren't provided default to a falsey-value that will fail an
# if-check (kind of like the example above).
control_parameter_example(
  FOO "foo value"
  BAZ "value"
  MULTI "a" "b" "c" "d"
  OPTION_A
)
```

So here's the thing... Why even prefix this?

Seriously, there is no point, just leave prefix empty always. You are usually
only going to call this command once within a function - so why invent a new
prefix name each and every time? Just let it be empty. In which case, the
variables will be set simply to `_${CONTROL_PARAMETER_NAME}`.

My recommendation is to follow a pattern kind of like this:

```cmake
function(control_parameter_example _POSITIONAL_PARAM_A _POSITIONAL_PARAM_B)
  cmake_parse_arguments(
    PARSE_ARGV   2
    #[[PREFIX]]  ""
    #[[OPTIONS]] "CHECKED"
    #[[ONEVAL]]  "INPUT;OUTPUT"
    #[[MULVAL]]  "DEPENDENCIES"
  )

  # Now all parameters are of the format _${PARAM_NAME}.
endfunction()
```

### "Can I Use Control Parameters With Macros?"

Yes, but it's tricky... You have to pass different parameters into the
`cmake_parse_arguments` command. I suggest reading about the function here:

https://cmake.org/cmake/help/latest/command/cmake_parse_arguments.html

### And More...

There are more things of note, but I think these are the most important things.
They give you the most context for the language, and hopefully with this you can
understand the examples provided.

From this point onward, please visit the projects in ascending order. Open the
CMakeLists.txt file in the example directory, and read it for instruction.
