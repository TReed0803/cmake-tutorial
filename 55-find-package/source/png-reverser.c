#include <stdio.h>
#include "png-helper.h"
#include "macros.h"

////////////////////////////////////////////////////////////////////////////////
// Main Entrypoint
////////////////////////////////////////////////////////////////////////////////

// -----------------------------------------------------------------------------
int main(int argc, char **argv) {
  if (argc != 3) {
    fprintf(stderr, "usage: png-reverser <file_in> <file_out>\n");
    return 1;
  }

  Png * png;
  LOG_AND_RETURN_IF_ERROR(1, create_png_from_file(argv[1], &png));
  LOG_AND_RETURN_IF_ERROR(1, png_flip_horizontal(png));
  LOG_AND_RETURN_IF_ERROR(1, png_write_to_file(png, argv[2]));
  destroy_png(png);

  return 0;
}
