#ifndef __MACROS_H
#define __MACROS_H

#include <errno.h>
#include <stdio.h>

////////////////////////////////////////////////////////////////////////////////
// Helper Macros
////////////////////////////////////////////////////////////////////////////////

// -----------------------------------------------------------------------------
#define LOG_AND_RETURN_IF_ERROR(error_if, check, ...)                           \
  if (check) {                                                                  \
    fprintf(stderr, "Failed check (" #error_if "): " __VA_ARGS__);              \
    return error_if;                                                            \
  }

// -----------------------------------------------------------------------------
#define LOG_AND_RETURN_IF_NULL(ptr)                                             \
  LOG_AND_RETURN_IF_ERROR(ENOMEM, !(ptr))

#endif // __MACROS_H
