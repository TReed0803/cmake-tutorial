#include <errno.h>
#include <stdint.h>
#include <stdlib.h>

#include "macros.h"
#include "png-helper.h"

////////////////////////////////////////////////////////////////////////////////
// Helper Functions
////////////////////////////////////////////////////////////////////////////////

// -----------------------------------------------------------------------------
static void swap32 (void * _a, void * _b) {
  uint32_t * a = (uint32_t *)_a;
  uint32_t * b = (uint32_t *)_b;
  uint32_t temp = *a;
  *a = *b;
  *b = temp;
}

// -----------------------------------------------------------------------------
static int init_png_with_file_context (Png * png, PngIoContext * ctx) {
  LOG_AND_RETURN_IF_ERROR(EFAULT, setjmp(png_jmpbuf(ctx->data)));

  png_set_sig_bytes(ctx->data, 8);
  png_read_info(ctx->data, ctx->info);
  png->width = png_get_image_width(ctx->data, ctx->info);
  png->height = png_get_image_height(ctx->data, ctx->info);
  png->color_type = png_get_color_type(ctx->data, ctx->info);
  png->bit_depth = png_get_bit_depth(ctx->data, ctx->info);
  png_read_update_info(ctx->data, ctx->info);

  LOG_AND_RETURN_IF_ERROR(EFAULT, setjmp(png_jmpbuf(ctx->data)));
  png->rows = (png_bytep*)malloc(sizeof(png_bytep) * png->height);
  for (unsigned y = 0; y < png->height; ++y) {
    png->rows[y] = (png_byte*)malloc(png_get_rowbytes(ctx->data, ctx->info));
  }
  png_read_image(ctx->data, png->rows);
  return 0;
}

// -----------------------------------------------------------------------------
static int png_write_with_context (Png const * png, PngIoContext * ctx) {
  LOG_AND_RETURN_IF_ERROR(EFAULT, setjmp(png_jmpbuf(ctx->data)));
  png_set_IHDR(ctx->data, ctx->info, png->width, png->height,
               png->bit_depth, png->color_type, PNG_INTERLACE_NONE,
               PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);
  png_write_info(ctx->data, ctx->info);
  LOG_AND_RETURN_IF_ERROR(EFAULT, setjmp(png_jmpbuf(ctx->data)));
  png_write_image(ctx->data, png->rows);
  LOG_AND_RETURN_IF_ERROR(EFAULT, setjmp(png_jmpbuf(ctx->data)));
  png_write_end(ctx->data, NULL);
  return 0;
}

////////////////////////////////////////////////////////////////////////////////
// Creation / Destruction (Heap)
////////////////////////////////////////////////////////////////////////////////

// -----------------------------------------------------------------------------
int create_png_from_file (char const * file, Png ** out) {
  Png * png = calloc(1, sizeof(Png));
  LOG_AND_RETURN_IF_NULL(png);
  int const error = init_png_from_file(png, file);
  if (error) {
    free(png);
    return error;
  }
  *out = png;
  return 0;
}

// -----------------------------------------------------------------------------
void destroy_png (Png * png) {
  deinit_png(png);
  free(png);
}

////////////////////////////////////////////////////////////////////////////////
// Init / Deinit (Stack)
////////////////////////////////////////////////////////////////////////////////

// -----------------------------------------------------------------------------
int init_png_with_file (Png * png, FILE * file) {
  char header[8];
  LOG_AND_RETURN_IF_ERROR(EIO, fread(header, 1, 8, file) != 8);
  LOG_AND_RETURN_IF_ERROR(ENOTSUP, png_sig_cmp(header, 0, 8));

  PngIoContext ctx;
  ctx.data = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
  if (!ctx.data) {
    return EINVAL;
  }
  ctx.info = png_create_info_struct(ctx.data);
  if (!ctx.info) {
    png_destroy_read_struct(&ctx.data, NULL, NULL);
    return EINVAL;
  }

  png_init_io(ctx.data, file);
  int const error = init_png_with_file_context(png, &ctx);
  png_destroy_read_struct(&ctx.data, &ctx.info, NULL);
  return error;
}

// -----------------------------------------------------------------------------
int init_png_from_file (Png * png, char const * file_name) {
  FILE *file = fopen(file_name, "rb");
  LOG_AND_RETURN_IF_ERROR(errno, !file);
  int const error = init_png_with_file(png, file);
  fclose(file);
  return error;
}

// -----------------------------------------------------------------------------
void deinit_png (Png * png) {
  for (unsigned y = 0; y < png->height; ++y) {
    free(png->rows[y]);
  }
  free(png->rows);
}

////////////////////////////////////////////////////////////////////////////////
// Operations
////////////////////////////////////////////////////////////////////////////////

// -----------------------------------------------------------------------------
int png_write_with_file (Png const * png, FILE * file) {
  PngIoContext ctx;
  ctx.data = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
  if (!ctx.data) {
    return EINVAL;
  }
  ctx.info = png_create_info_struct(ctx.data);
  if (!ctx.info) {
    png_destroy_write_struct(&ctx.data, NULL);
    return EINVAL;
  }

  png_init_io(ctx.data, file);
  int const error = png_write_with_context(png, &ctx);
  png_destroy_write_struct(&ctx.data, &ctx.info);
  return error;
}

// -----------------------------------------------------------------------------
int png_write_to_file (Png const * png, char const * file_name) {
  FILE *file = fopen(file_name, "wb");
  LOG_AND_RETURN_IF_ERROR(errno, !file);
  int const error = png_write_with_file(png, file);
  fclose(file);
  return error;
}

// -----------------------------------------------------------------------------
int png_flip_horizontal (Png * png) {
  LOG_AND_RETURN_IF_ERROR(ENOTSUP, png->color_type != PNG_COLOR_TYPE_RGBA);
  unsigned const halfway_point = png->width / 2;
  for (unsigned y = 0; y < png->height; ++y) {
    png_byte * row = png->rows[y];
    for (unsigned x = 0; x < halfway_point; ++x) {
      png_byte * front = &(row[x * 4]);
      png_byte * back = &(row[(png->width - x - 1) * 4]);
      swap32(front, back);
    }
  }
  return 0;
}
