#include <stdlib.h>
#include <stdint.h>

#include <SDL2/SDL.h>

#include "macros.h"
#include "png-helper.h"

// -----------------------------------------------------------------------------
int main (int argc, char * argv[]) {
  Png * png;
  LOG_AND_RETURN_IF_ERROR(1, create_png_from_file(argv[1], &png));

  SDL_Renderer *renderer;
  SDL_Window *window;
  LOG_AND_RETURN_IF_ERROR(1, SDL_Init(SDL_INIT_TIMER | SDL_INIT_VIDEO));
  LOG_AND_RETURN_IF_ERROR(1, SDL_CreateWindowAndRenderer(
    /*width=*/500,
    /*height=*/500,
    /*flags=*/0,
    /*window=*/&window,
    /*renderer=*/&renderer
  ));

  SDL_Surface *surface = SDL_CreateRGBSurfaceWithFormat(
    /*flags=*/0,
    /*width=*/png->width,
    /*height=*/png->height,
    /*bpp=*/32,
    /*format=*/SDL_PIXELFORMAT_RGBA32
  );
  LOG_AND_RETURN_IF_NULL(surface);
  LOG_AND_RETURN_IF_ERROR(1, SDL_LockSurface(surface));
  for (unsigned y = 0; y < png->height; ++y) {
    png_byte* in_row = png->rows[y];
    uint8_t* out_row = (uint8_t*)(surface->pixels + y * surface->pitch);
    for (unsigned x = 0;  x < png->width; ++x) {
      png_byte* in_pixel = &in_row[x * 4];
      uint8_t*  out_pixel = out_row + x * surface->format->BytesPerPixel;
      out_pixel[0] = in_pixel[0];
      out_pixel[1] = in_pixel[1];
      out_pixel[2] = in_pixel[2];
      out_pixel[3] = in_pixel[3];
    }
  }
  SDL_UnlockSurface(surface);

  SDL_Texture * texture = SDL_CreateTextureFromSurface(renderer, surface);
  LOG_AND_RETURN_IF_NULL(texture);
  SDL_FreeSurface(surface);

  SDL_Event event;
  while (1) {
    LOG_AND_RETURN_IF_ERROR(1, SDL_RenderCopy(renderer, texture, NULL, NULL));
    SDL_RenderPresent(renderer);
    if (SDL_PollEvent(&event) && event.type == SDL_QUIT) {
      break;
    }
  }

  SDL_DestroyTexture(texture);
  SDL_DestroyRenderer(renderer);
  SDL_DestroyWindow(window);
  SDL_Quit();
  destroy_png(png);
  return EXIT_SUCCESS;
}
