# Alright, this is the first time we are just raw including a CMake file!
# The only thing you really need to know is when you write utilities like this,
# you should always wrap them in an include guard (much like C or C++).
#
# CMake has a native function for this:
include_guard()

# This is new - find_package is something I'd like to skip discussing in detail
# for now. However, all you need to know is it kind of works like a module,
# except it's a module which depends on other things which may or may not be
# installed on the system.
#
# So git may or may not be installed on the system. And if it's not, this call
# will set a variable GIT_FOUND to a falsey value (for future if-checks).
#
# For now, all you need to know is that this sets the following variables:
#   GIT_FOUND      := Whether or not git was found.
#   GIT_EXECUTABLE := Path to the git executable.
find_package(Git)

# And here is our first function.
#
# Why we need this function requires some explanation of git itself. Basically,
# you can think of a submodule as a pointer to an external commit (like, on
# another repo entirely).
#
# By default, when you pull a git repo, git does not assume you want to resolve
# all these pointers (and for good reason - what if you have a bunch of optional
# stuff? We wouldn't want to waste all that time pulling it).
#
# So what this function does is updates the submodule at the provided path,
# relative to the root of the project source. The --init flag also initializes
# if needed.
#
# NOTE: Initialize is usually the first step - it means download if needed.
#       You could imagine as you develop code that the "pointer" might change,
#       someone may update the git submodule to point to a new release of some
#       software. Update is more about updating our local copy to match whatever
#       the pointer currently says. So yeah, this just does both of those steps
#       always.
function(git_submodule_check _PATH)
  # Option support allows us to skip the submodule update if set.
  if(NOT FACTORIAL_UPDATE_SUBMODULES)
    return()
  endif()

  # If we got this far, we want to update the submodule, but we can't if git was
  # not found. We should FATAL_ERROR. This stops the configuration process.
  if(NOT GIT_FOUND)
    message(FATAL_ERROR "No git executable found.")
  endif()

  # And finally, we want to just call git to do the submodule update, and check
  # that the result of the call is okay.
  execute_process(
    COMMAND ${GIT_EXECUTABLE} submodule update --init "${_PATH}"
    WORKING_DIRECTORY "${CMAKE_SOURCE_DIR}"
    RESULT_VARIABLE result
  )
  if(NOT result EQUAL 0)
    message(FATAL_ERROR "Failed to update git submodule at ${_PATH}.")
  endif()
endfunction()
