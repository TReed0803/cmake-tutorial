#include <limits>

#include <gtest/gtest.h>
#include <factorial.h>

TEST(Factorial, WorksFor0) {
  EXPECT_EQ(factorial(0), 1);
}

TEST(Factorial, DetectsOverflow) {
  EXPECT_EQ(factorial(std::numeric_limits<unsigned>::max()), 0);
}

TEST(Factorial, WorksForAFewNumbers) {
  EXPECT_EQ(factorial(1), 1);
  EXPECT_EQ(factorial(2), 2);
  EXPECT_EQ(factorial(3), 6);
  EXPECT_EQ(factorial(4), 24);
  EXPECT_EQ(factorial(5), 120);
}
