#include <stdio.h>

int main (int argc, char const * argv[]) {
  if (argc < 2) {
    fprintf(stderr, "Failed to provide enough parameters.\n");
    return 1;
  }
  FILE *output = fopen(OUTPUT_FILE, "w");
  if (output == NULL) {
    fprintf(stderr, "Failed to open the file for writing.\n");
    return 1;
  }
  fprintf(output, "#include <stdio.h>\n#include<string.h>\n\n");
  fprintf(output, "int main (int argc, char const * argv[]) {\n");
  fprintf(output, "  if (argc < 2) {\n");
  fprintf(output, "    fprintf(stderr, \"Not enough input provided.\\n\");\n");
  fprintf(output, "    return 1;\n");
  fprintf(output, "  }\n");
  fprintf(output, "  for (int idx = 1; idx < argc; ++idx) {\n");
  for (int idx = 1; idx < argc; ++idx) {
    fprintf(output, "    if (strcmp(\"%s\", argv[idx]) == 0) continue;\n", argv[idx]);
  }
  fprintf(output, "    fprintf(stderr, \"Invalid input provided.\\n\");\n");
  fprintf(output, "    return 1;\n");
  fprintf(output, "  }\n  return 0;\n}\n");
  return 0;
}
